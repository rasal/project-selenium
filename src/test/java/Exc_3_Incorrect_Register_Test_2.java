import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class Exc_3_Incorrect_Register_Test_2 extends SeleniumBaseTest{

    @DataProvider
    public static Object [][] wrongPasswords() {
        return new Object[][] {
                {"tester1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Tester!", "Passwords must have at least one digit ('0'-'9')."},
                {"Tester1", "Passwords must have at least one non alphanumeric character."}
        };
    }

    @Test(dataProvider = "wrongPasswords")
    public void IncorrectRegisterTest2 (String password, String expErrorMessage) {

        new LoginPage(driver)

                .clickGoToRegister();

        new CreateAccountPage(driver)

                .typeEmail0("test@test.com")
                .typePassword0(password)
                .typeConfirmPassword0(password)
                .submitRegisterWithFailure()
                .assertPasswordErrorMessage(expErrorMessage);
    }
}

