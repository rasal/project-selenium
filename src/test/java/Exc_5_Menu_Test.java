import org.testng.annotations.Test;

public class Exc_5_Menu_Test extends SeleniumBaseTest {

    @Test
    public void MenuTest() {

        new LoginPage(driver)

                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()

                .goToProcesses()
                .assertProcessesUrl("http://localhost:4444/Projects")
                .assertProcessesHeader()

                .goToCharacteristics()
                .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
                .assertCharacteristicsHeader()

                .goToDashboard()
                .assertDashboardUrl("http://localhost:4444/")
                .assertDashboardHeader();
    }
}


