import org.testng.annotations.Test;

import java.util.UUID;

public class Exc_7_Process_Add_Test_2 extends SeleniumBaseTest {

    @Test
    public void ProcessAddTest() {
        String processName = UUID.randomUUID().toString().substring(0, 10);

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName = "Processor")
                .submitCreate()
                .goToDashboard()
                .assertProcessOnDashboard(processName);
    }
}
