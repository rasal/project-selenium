import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class Exc_1_Correct_Register_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getEmails() {
        return new Object[][]{
                {"tester@test.pl"},
                {"testerka@test.pl"},
                {"toster@test.pl"}
        };
    }

    @Test(dataProvider = "getEmails")
    public void CorrectRegisterTest(String Email) {


        new LoginPage(driver)

                .clickGoToRegister();

        new CreateAccountPage(driver)

                .typeEmail0(Email)
                .typePassword0("Toster123!")
                .typeConfirmPassword0("Toster123!")
                .submitRegister()
                .assertRegisterSuccess()
                .assertRegisterUrl("http://localhost:4444/");

        /*???????*/
    }
}


