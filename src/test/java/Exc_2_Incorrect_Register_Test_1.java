import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class Exc_2_Incorrect_Register_Test_1 extends SeleniumBaseTest{

        @DataProvider
        public Object[][] getEmails() {
            return new Object[][] {
                    {"test@test.pl"},
                    {"tester@test.pl"},
                    {"testerka@test.pl"}
            };
        }

        @Test(dataProvider = "getEmails")
        public void IncorrectRegisterTest1 (String Email) {

            new LoginPage(driver)

                    .clickGoToRegister();

            new CreateAccountPage(driver)

                    .typeEmail0(Email)
                    .typePassword0("Toster123!")
                    .typeConfirmPassword0("Toster123")
                    .submitRegisterWithFailure()
                    .isErrorMessageShow();
        }
    }

