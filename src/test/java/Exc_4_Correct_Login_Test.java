import org.testng.annotations.Test;

public class Exc_4_Correct_Login_Test extends SeleniumBaseTest {

    @Test
    public void CorrectLoginTest() {

        new LoginPage(driver)

                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertLoginSuccess("http://localhost:4444/");
    }
}
