import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashboardPage extends HomePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h2[text()='DEMO PROJECT']")
    private WebElement demoProject;


    public DashboardPage assertDashboardUrl(String expUrl) {
        junit.framework.Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }

    public DashboardPage assertDashboardHeader() {
        Assert.assertTrue(demoProject.isDisplayed());
        return this;
    }


    private final String GENERIC_PROCESS_ROW_XPATH = "//div[1]/div/div[3]/div/div[4]";

    public DashboardPage assertProcessOnDashboard(String expName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);
        return this;
    }
}


