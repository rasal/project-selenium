import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateProcessPage extends HomePage {

    public CreateProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#Name")
    private WebElement processType;

    @FindBy(css = ".btn.btn-success")
    private WebElement submitBtn;


    public CreateProcessPage typeName(String processName) {
        processType.sendKeys(processName);
        return this;
    }

    public ProcessesPage submitCreate() {
        submitBtn.click();
        return new ProcessesPage(driver);
    }
}
