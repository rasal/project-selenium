import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage extends SeleniumPage{

    public HomePage(WebDriver driver) {
      super(driver);
    }

    @FindBy(css = ".User")
    private WebElement emailAdress;

    @FindBy(css = ".profile_info>h2")
    public WebElement welcomeElm;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    @FindBy(linkText = "Dashboard")
    private WebElement dashboardMenu;

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css = "a[href$=Projects]")
    private WebElement processesMenu;

    @FindBy(css = "a[href$=Characteristics]")
    private WebElement characteristicsMenu;


    public HomePage assertRegisterSuccess() {
        Assert.assertTrue(welcomeElm.isDisplayed());
        Assert.assertEquals(welcomeElm.getText(), "Welcome\ntester@test.pl");
        Assert.assertEquals(welcomeElm.getText(), "Welcome\ntesterka@test.pl");
        Assert.assertEquals(welcomeElm.getText(), "Welcome\ntoster@test.pl");
        return this;
    }

    public HomePage assertRegisterUrl(String expUrl) {
        junit.framework.Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }

    public HomePage assertLoginSuccess(String expUrl) {
        Assert.assertEquals(welcomeElm.getText(), "Welcome\ntest@test.com");
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }


    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));
        return parent.getAttribute("class").contains("active");
    }


    public DashboardPage goToDashboard() {
        if (!isParentExpanded(homeNav))
            homeNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));
        dashboardMenu.click();
        return new DashboardPage(driver);
    }


    public ProcessesPage goToProcesses() {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));
        processesMenu.click();
        return new ProcessesPage(driver);
    }


    public CharacteristicsPage goToCharacteristics() {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(characteristicsMenu));
        characteristicsMenu.click();
        return new CharacteristicsPage(driver);
    }
}

