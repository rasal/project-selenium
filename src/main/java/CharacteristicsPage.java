import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CharacteristicsPage extends HomePage{
    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".page-title h3")
    private WebElement pageHeader;


    public CharacteristicsPage assertCharacteristicsUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }

    public CharacteristicsPage assertCharacteristicsHeader() {
        Assert.assertEquals(pageHeader.getText(), "Characteristics");
        return this;
    }
}
