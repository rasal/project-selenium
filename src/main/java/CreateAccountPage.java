import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateAccountPage {

    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = ".btn")
    private WebElement submitRegisterBtn;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement passwordError;

    @FindBy(css = ".validation-summary-errors>ul>li")
    private WebElement passwordErrorShow;


    public CreateAccountPage typeEmail0(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword0(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage typeConfirmPassword0(String password) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitRegister() {
        submitRegisterBtn.click();
        return new HomePage(driver);
    }

    public CreateAccountPage submitRegisterWithFailure() {
        submitRegisterBtn.click();
        return new CreateAccountPage(driver);
    }

    public CreateAccountPage isErrorMessageShow() {
        Assert.assertTrue(passwordError.isDisplayed());
        Assert.assertEquals(passwordError.getText(), "The password and confirmation password do not match.");
        return this;
    }

    public CreateAccountPage assertPasswordErrorMessage(String expError) {
        Assert.assertTrue(passwordErrorShow.isDisplayed());
        Assert.assertEquals(passwordErrorShow.getText(), expError);
        return this;
    }
}
